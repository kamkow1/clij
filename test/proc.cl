
macro STDOUT 1 endmacro

macro SYS_WRITE 1  endmacro
macro SYS_EXIT  60 endmacro

-- fd buf len
macro io-write SYS_WRITE syscall3 endmacro

-- ptr
macro io-print-char 1 swap STDOUT io-write endmacro

proc io-print
    swap
    dup
    1 loop
        memread print pop
        1 add
        memread 0 neq
    endloop
    pop
    swap
endproc

proc io-println
    #param 1
    io-print
    10 print pop
    pop
endproc

mem str-count-result 1
proc str-count
    #param 1

    #addr str-count-result 0 memwrite pop pop

    loop memread 0 neq do
        #addr str-count-result memread 1 add memwrite pop pop

        memread print pop
        #addr str-count-result memread 48 add print pop pop

        dup 1 add swap pop
        memread 33 neq
    endloop
    pop
endproc

proc main
--    5 "Hello STDOUT io-write
    "XX! str-count pop
-- #addr str-count-result memread 48 add print pop pop pop
endproc

