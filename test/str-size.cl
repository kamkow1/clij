
proc main
    memval byte hello-string "Hello

    memval int64 counter 0
    memval int64 pointer 0    

    loop #mem hello-string memread 0 neq do
        #mem hello-string memread print pop
        #mem hello-string #mem pointer 1 add assign
    endloop

endproc
