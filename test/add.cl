include "#std/std.cl"


macro new-line
    10 print
endmacro

96 1 add print

10 print

31 32 33

pop
pop
pop

1 2 add
dup
48 add
print

new-line

0 1 eq 48 add
print
io-new-line

0 if
    1 if
        98 print
        new-line
    endif
    97 print
    new-line
endif

macro parent
    macro child
        99 print
    endmacro
endmacro

parent

child

1 loop
    2 3 neq if 1 continue endif
    100 print

endloop

