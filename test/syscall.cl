
macro STDOUT 1 endmacro

macro SYS_WRITE 1  endmacro
macro SYS_EXIT  60 endmacro

-- fd buf len
macro io-write SYS_WRITE syscall3 endmacro

-- ptr
macro io-print-char 1 swap STDOUT io-write endmacro

-- ptr
proc io-print
    dup
    1 loop
        dup
        io-print-char
        1 add
        memread 0 neq
    endloop
endproc

-- ptr
proc io-println
    io-print
    #charlit 10 io-print
endproc

"Hello io-println

