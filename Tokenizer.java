import java.util.ArrayList;
import java.util.Set;
import java.lang.Character;

public class Tokenizer {
    private static final Set<String> KEYWORDS = Set.of(
        "add", "sub",
        "print",
        "pop", "dup", "swap",
        "eq", "neq",
        "lt", "gt",
        "le", "ge",
        "macro", "endmacro",
        "if", "endif",
        "loop", "endloop",
        "do",
        "break", "continue",
        "syscall",
        "syscall1", "syscall2",
        "syscall3", "syscall4",
        "syscall5", "syscall6",
        "include",
        "memread", "memwrite",
        "memalloc", "memdealloc",
        "mem", "memval",
        "proc", "endproc",
        "assign"
    );

    private static boolean isInt(String s) {
        if (s == null) { return false; }

        int l = s.length();
        if (l == 0) { return false; }

        int i = 0;
        if (s.charAt(0) == '-') {
            if (l == 1) { return false; }
            i = 1;
        }
        for(;i < l; i++) {
            char c = s.charAt(i);
            if (c < '0' || c > '9') { return false; }
        }
        return true;
    }

    public ArrayList<Token> tokenize(String source) {
        ArrayList<Token> tokens = new ArrayList<Token>();

        String newSource = "";
        String[] lines = source.split("\n");
        for (int i = 0; i < lines.length; i++) {
            if (lines[i].startsWith("--")) { continue; } // skip the comments
            newSource += lines[i] + " ";
        }
        source = newSource;

        String[] words = source.split("\\s+");
        for (int i = 0; i < words.length; i++) {
            String word = words[i];
            if (word.length() == 0) { continue; }

            if (isInt(word)) {
                Token token = new Token(word);
                token.type = TokenType.INT;
                tokens.add(token);
            } else if (KEYWORDS.contains(word)) {
                Keyword kw = new Keyword(word);
                kw.type = TokenType.KEYWORD;

                for (KeywordType kwtype: KeywordType.values()) {
                    if (kwtype.toString().toLowerCase().equals(word.toLowerCase())) {
                        kw.kwtype = kwtype;
                    }
                }

                tokens.add(kw);
            } else if (word.charAt(0) == '"') {
                word = word.substring(1, word.length());
                Token token = new Token(word);
                token.type = TokenType.STRING;
                tokens.add(token);
            } else if(word.charAt(0) == '#') {
                word = word.substring(1, word.length());
                Directive directive = new Directive(word);
                directive.type = TokenType.DIRECTIVE;
                for (DirectiveType dirtype: DirectiveType.values()) {
                    if (dirtype.toString().toLowerCase().equals(word.toLowerCase())) {
                        directive.dirtype = dirtype;
                    }
                }
                tokens.add(directive);
            } else {
                Token token = new Token(word);
                token.type = TokenType.IDENT;
                tokens.add(token);
            }
        }

        return tokens;
    }
}

