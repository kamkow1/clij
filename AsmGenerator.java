import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class AsmGenerator {
    public HashMap<String, Macro> macros = new HashMap<String, Macro>();
    private int labelCounter = 0;
    private int stringCounter = 0;
    private Map<KeywordType, String> cmpTypeToX86InstrMap = Map.ofEntries(
        Map.entry(KeywordType.EQ,  "sete"),
        Map.entry(KeywordType.NEQ, "setne"),
        Map.entry(KeywordType.LT,  "setl"),
        Map.entry(KeywordType.GT,  "setg"),
        Map.entry(KeywordType.LE,  "setle"),
        Map.entry(KeywordType.GE,  "setge")
    );
    private Map<String, String> defSizes = Map.ofEntries(
        Map.entry("int64", "dq"),
        Map.entry("int32", "dd"),
        Map.entry("int16", "dw"),
        Map.entry("byte",  "db")
    );
    private Stack<Integer> endifCounterStack = new Stack<Integer>();
    private Stack<Integer> loopCounterStack = new Stack<Integer>();
    private Stack<Macro> macroStack = new Stack<Macro>();

    public Program generate(String currentFile, ArrayList<Token> tokens, boolean generateStartAndEnd) throws IOException {

        Program program = new Program();

        if (generateStartAndEnd) {
            program.code += "section .text\n";
            program.data += "section .data\n";
            program.bss  += "section .bss\n";
        }

        for (int i = 0; i < tokens.size(); i++) {
            Token token = tokens.get(i);
            switch (token.type) {
                case INT:
                    if (!this.macroStack.empty()) {
                        this.macroStack.peek().addToken(token);
                        break;
                    }
                    program.code += "\n; " + token.text + "\n";
                    program.code += "push " + token.text + "\n";
                    break;
                case STRING:
                    if (!this.macroStack.empty()) {
                        this.macroStack.peek().addToken(token);
                        break;
                    }
                    int stringLabel = ++this.stringCounter;
                    program.data += "string" + stringLabel + ": db \"" + token.text + "\", 0\n";
                    program.code += "\n; \"" + token.text + "\n";
                    program.code += "push string" + stringLabel + "\n";
                    break;
                case IDENT:
                    if (!this.macroStack.empty()) {
                        this.macroStack.peek().addToken(token);
                        break;
                    }
                    if (macros.containsKey(token.text)) {
                        Macro macro = macros.get(token.text);
                        Program macroProgram = this.generate(currentFile, macro.tokens(), false);
                        program.code += "\n; MACRO " + token.text + "\n" + macroProgram.code;
                        program.data += macroProgram.data;
                        program.bss  += macroProgram.bss;
                    } else {
                        program.code += "\n; call " + token.text + "\n";
                        program.code += "call " + token.text.replace("-", "_") + "\n";
                    }
                    break;
                case DIRECTIVE:
                    if (!this.macroStack.empty()) {
                        this.macroStack.peek().addToken(token);
                        token = tokens.get(++i);
                        this.macroStack.peek().addToken(token);
                        break;
                    }
                    Directive dir = (Directive)token;
                    token = tokens.get(++i);
                    switch (dir.dirtype) {
                        case CHARLIT:
                            int stringLabel1 = ++this.stringCounter;
                            program.data += "string" + stringLabel1 + ": db " + token.text + ", 0\n";
                            program.code += "push string" + stringLabel1 + "\n";
                            break;
                        case PARAM:
                            int paramNum = Integer.parseInt(token.text);
                            paramNum *= 8;
                            program.code += "mov rax, [rsp+" + paramNum + "]\n";
                            program.code += "push rax\n";
                            break;
                        case MEM:
                            program.code += "push " + token.text.replace("-", "_") + "\n";
                            break;
                        default:
                            System.out.println("Unhandled directive type " + dir.text);
                            System.exit(1);
                            break;
                    }
                    break;
                case KEYWORD:
                    Keyword kw = (Keyword)token;
                    switch (kw.kwtype) {
                        case PROC:
                            if (!this.macroStack.empty()) {
                                this.macroStack.peek().addToken(token);
                                token = tokens.get(++i);
                                this.macroStack.peek().addToken(token);
                                break;
                            }
                            token = tokens.get(++i);
                            String name = token.text.replace("-", "_");
                            program.code += "\n; proc " + name + "\n";
                            program.code += name + ":\n";
                            break;
                        case ENDPROC:
                            if (!this.macroStack.empty()) {
                                this.macroStack.peek().addToken(token);
                                break;
                            }
                            program.code += "\n; endproc\n";
                            program.code += "ret\n";
                            break;
                        case SWAP:
                            if (!this.macroStack.empty()) {
                                this.macroStack.peek().addToken(token);
                                break;
                            }
                            program.code += "\n; swap\n";
                            program.code += "pop rax\n";
                            program.code += "pop rdx\n";
                            program.code += "push rax\n";
                            program.code += "push rdx\n";
                            break;
                        case ASSIGN:
                            if (!this.macroStack.empty()) {
                                this.macroStack.peek().addToken(token);
                                break;
                            }
                            program.code += "\n; assign\n";
                            program.code += "pop rax\n";
                            program.code += "pop rdx\n";
                            program.code += "mov [rdx], rax\n";
                            break;
                        case MEM:
                            if (!this.macroStack.empty()) {
                                this.macroStack.peek().addToken(token);
                                token = tokens.get(++i);
                                this.macroStack.peek().addToken(token);
                                token = tokens.get(++i);
                                this.macroStack.peek().addToken(token);
                                break;
                            }
                            token = tokens.get(++i);
                            String name1 = token.text.replace("-", "_");
                            token = tokens.get(++i);
                            String size = token.text;
                            program.bss += name1 + ": resb " + size + "\n";
                            break;
                        case MEMVAL:
                            if (!this.macroStack.empty()) {
                                this.macroStack.peek().addToken(token);
                                token = tokens.get(++i);
                                this.macroStack.peek().addToken(token);
                                token = tokens.get(++i);
                                this.macroStack.peek().addToken(token);
                                token = tokens.get(++i);
                                this.macroStack.peek().addToken(token);
                                break;
                            }
                            token = tokens.get(++i);
                            String defSize = this.defSizes.get(token.text);
                            if (defSize == null) {
                                defSize = "db";
                            }
                            token = tokens.get(++i);
                            String name2 = token.text.replace("-", "_");
                            token = tokens.get(++i);
                            if (token.type == TokenType.STRING) {
                                program.data += name2 + ": " + defSize +  " \"" + token.text + "\"\n";
                            } else {
                                program.data += name2 + ": " + defSize + " " + token.text + "\n";
                            }
                            break;
                        case MEMREAD:
                            if (!this.macroStack.empty()) {
                                this.macroStack.peek().addToken(token);
                                break;
                            }
                            program.code += "\n; memread\n";
                            program.code += "pop rax\n";
                            program.code += "mov rdx, [rax]\n";
                            program.code += "push rdx\n";
                            break;
                        case MEMWRITE:
                            if (!this.macroStack.empty()) {
                                this.macroStack.peek().addToken(token);
                                break;
                            }
                            program.code += "\n; memwrite\n";
                            // program.code += "pop rax\n";
                            // program.code += "pop rdi\n";
                            program.code += "mov rax, [rsp]\n";   // src
                            program.code += "mov rdi, [rsp+8]\n"; // dest
                            program.code += "mov [rdi], rax\n";
                            break;
                        case SYSCALL6:
                            if (!this.macroStack.empty()) {
                                this.macroStack.peek().addToken(token);
                                break;
                            }
                            program.code += "\n; syscall6\n";
                            program.code += "pop rax\n"; // syscall no
                            program.code += "pop rdi\n"; // arg1
                            program.code += "pop rsi\n"; // arg2
                            program.code += "pop rdx\n"; // arg3
                            program.code += "pop r10\n"; // arg4
                            program.code += "pop r8\n";  // arg5
                            program.code += "pop r9\n";  // arg6
                            program.code += "syscall\n";
                            break;
                        case SYSCALL5:
                            if (!this.macroStack.empty()) {
                                this.macroStack.peek().addToken(token);
                                break;
                            }
                            program.code += "\n; syscall5\n";
                            program.code += "pop rax\n"; // syscall no
                            program.code += "pop rdi\n"; // arg1
                            program.code += "pop rsi\n"; // arg2
                            program.code += "pop rdx\n"; // arg3
                            program.code += "pop r10\n"; // arg4
                            program.code += "pop r8\n";  // arg5
                            program.code += "syscall\n";
                            break;
                        case SYSCALL4:
                            if (!this.macroStack.empty()) {
                                this.macroStack.peek().addToken(token);
                                break;
                            }
                            program.code += "\n; syscall4\n";
                            program.code += "pop rax\n"; // syscall no
                            program.code += "pop rdi\n"; // arg1
                            program.code += "pop rsi\n"; // arg2
                            program.code += "pop rdx\n"; // arg3
                            program.code += "pop r10\n"; // arg4
                            program.code += "syscall\n";
                            break;
                        case SYSCALL3:
                            if (!this.macroStack.empty()) {
                                this.macroStack.peek().addToken(token);
                                break;
                            }
                            program.code += "\n; syscall3\n";
                            program.code += "pop rax\n"; // syscall no
                            program.code += "pop rdi\n"; // arg1
                            program.code += "pop rsi\n"; // arg2
                            program.code += "pop rdx\n"; // arg3
                            program.code += "syscall\n";
                            break;
                        case SYSCALL2:
                            if (!this.macroStack.empty()) {
                                this.macroStack.peek().addToken(token);
                                break;
                            }
                            program.code += "\n; syscall2\n";
                            program.code += "pop rax\n"; // syscall no
                            program.code += "pop rdi\n"; // arg1
                            program.code += "pop rsi\n"; // arg2
                            program.code += "syscall\n";
                            break;
                        case SYSCALL1:
                            if (!this.macroStack.empty()) {
                                this.macroStack.peek().addToken(token);
                                break;
                            }
                            program.code += "\n; syscall1\n";
                            program.code += "pop rax\n"; // syscall no
                            program.code += "pop rdi\n"; // arg1
                            program.code += "syscall\n"; // syscall
                            break;
                        case SYSCALL:
                            if (!this.macroStack.empty()) {
                                this.macroStack.peek().addToken(token);
                                break;
                            }
                            program.code += "\n; syscall\n";
                            program.code += "pop rax\n"; // syscall no
                            program.code += "syscall\n"; // syscall
                            break;
                        case BREAK:
                            if (!this.macroStack.empty()) {
                                this.macroStack.peek().addToken(token);
                                break;
                            }
                            int loopLabel2 = this.loopCounterStack.peek();
                            program.code += "\n; break\n";
                            program.code += "jmp .endloop" + loopLabel2 + "\n"; // break the loop
                            break;
                        case CONTINUE:
                            if (!this.macroStack.empty()) {
                                this.macroStack.peek().addToken(token);
                                break;
                            }
                            int loopLabel3 = this.loopCounterStack.peek();
                            program.code += "\n; continue\n";
                            program.code += "jmp .preloop" + loopLabel3 + "\n"; // continue the loop
                            break;
                        case ENDLOOP:
                            if (!this.macroStack.empty()) {
                                this.macroStack.peek().addToken(token);
                                break;
                            }
                            int loopLabel1 = this.loopCounterStack.pop();
                            program.code += "\n; endloop\n";
                            program.code += "jmp .preloop" + loopLabel1 + "\n";
                            program.code += ".endloop" + loopLabel1 + ":\n";
                            break;
                        case LOOP:
                            if (!this.macroStack.empty()) {
                                this.macroStack.peek().addToken(token);
                                while (!token.text.equals("do") && i < tokens.size()) {
                                    this.macroStack.peek().addToken(token);
                                    token = tokens.get(++i);
                                }
                                token = tokens.get(++i);
                                this.macroStack.peek().addToken(token);
                                break;
                            }
                            int loopLabel = ++this.labelCounter;
                            this.loopCounterStack.push(loopLabel);
                            ArrayList<Token> cond = new ArrayList<Token>();
                            token = tokens.get(++i);
                            while (!token.text.equals("do") && i < tokens.size()-1) {
                                cond.add(token);
                                token = tokens.get(++i);
                            }
                            // token = tokens.get(++i); // skip do
                            Program condProgram = this.generate(currentFile, cond, false);
                            program.data += condProgram.data;
                            program.bss  += condProgram.bss;

                            program.code += "\n; loop\n";
                            program.code += ".preloop" + loopLabel + ":\n";
                            program.code += condProgram.code;
                            program.code += "pop rax\n";
                            program.code += "cmp rax, 1\n";
                            program.code += "jne .endloop" + loopLabel + "\n";
                            program.code += ".loop" + loopLabel + ":\n";

                            // program.code += ".preloop" + loopLabel + ":\n";
                            // program.code += "pop rax\n";                       // get the condition
                            // program.code += "cmp rax, 0\n";                    // check if false
                            // program.code += "jne .loop" + loopLabel + "\n";    // jump to loop if not false
                            // program.code += "jmp .endloop" + loopLabel + "\n"; // skip
                            // program.code += ".loop" + loopLabel + ":\n";
                            break;
                        case EQ: case NEQ:
                        case LT: case GT:
                        case LE: case GE:
                            if (!this.macroStack.empty()) {
                                this.macroStack.peek().addToken(token);
                                break;
                            }
                            String cmpInstr = cmpTypeToX86InstrMap.get(kw.kwtype);
                            program.code += "\n; " + token.text + "\n";
                            program.code += "pop rdi\n";        // op1
                            program.code += "pop rdx\n";        // op2
                            program.code += "xor rax, rax\n";   // clear rax
                            program.code += "cmp rdi, rdx\n";   // compare
                            program.code += cmpInstr + " al\n"; // set the result
                            program.code += "push rax\n";       // push it back
                            break;
                        case ENDIF:
                            if (!this.macroStack.empty()) {
                                this.macroStack.peek().addToken(token);
                                break;
                            }
                            program.code += "\n; endif\n";
                            program.code += ".endif" + (this.endifCounterStack.pop()) + ":\n";
                            break;
                        case IF:
                            if (!this.macroStack.empty()) {
                                this.macroStack.peek().addToken(token);
                                break;
                            }
                            int ok = ++labelCounter;
                            int endif = ++labelCounter;
                            this.endifCounterStack.push(endif);

                            program.code += "\n; if\n";
                            program.code += "pop rax\n";                 // op1
                            program.code += "cmp rax, 1\n";              // compare
                            program.code += "je .ok" + ok + "\n";        // jump if ok
                            program.code += "jmp .endif" + endif + "\n"; // jump to endif
                            program.code += ".ok" + ok + ":\n";
                            break;
                        case INCLUDE:
                            if (!this.macroStack.empty()) {
                                ArrayList<Token> includeTokens = new ArrayList<Token>();
                                includeTokens.add(token);
                                token = tokens.get(++i);
                                includeTokens.add(token);
                                this.macroStack.peek().addTokens(includeTokens);
                                break;
                            }
                            token = tokens.get(++i);
                            if (token.type != TokenType.STRING) {
                                System.out.println(currentFile + ": include directive must be followed by a string");
                                System.exit(1);
                            }
                            String path = token.text;

                            if (path.startsWith("#std")) {
                                String stdPath = System.getenv("CLIJSTD");
                                if (stdPath == null) {
                                    System.out.println(currentFile + ": #std is used but CLIJSTD is not set");
                                    System.exit(1);
                                }
                                path = path.replaceFirst(Pattern.quote("#std"), Matcher.quoteReplacement(stdPath));
                            } else if (path.startsWith("#root")) {
                                String root = System.getProperty("user.dir");
                                path = path.replaceFirst(Pattern.quote("#root"), Matcher.quoteReplacement(root));
                            }

                            File file = new File(path);
                            FileInputStream fis = new FileInputStream(file);
                            byte[] fileData = new byte[(int)file.length()];
                            fis.read(fileData);
                            fis.close();
                            String source_file = new String(fileData, "UTF-8");

                            Tokenizer tokenizer = new Tokenizer();
                            ArrayList<Token> newTokens = tokenizer.tokenize(source_file);

                            AsmGenerator asmGen = new AsmGenerator();
                            Program p = asmGen.generate(currentFile, newTokens, false);

                            this.macros.putAll(asmGen.macros);
                            program.code += p.code;
                            program.data += p.data;
                            program.bss  += p.bss;

                            break;
                        case ENDMACRO:
                            Macro macro = this.macroStack.pop();
                            macros.put(macro.name(), macro);
                            break;
                        case MACRO:
                            token = tokens.get(++i);
                            if (token.type != TokenType.IDENT) {
                                System.out.println(currentFile + ": macro declaration must be followed by it's name");
                                System.exit(1);
                            }
                            String macroName = token.text;
                            this.macroStack.push(new Macro(macroName, new ArrayList<Token>()));
                            break;
                        case SUB:
                            if (!this.macroStack.empty()) {
                                this.macroStack.peek().addToken(token);
                                break;
                            }
                            program.code += "\n; sub\n";
                            program.code += "pop rax\n";      // pop into rax - op1
                            program.code += "pop rdx\n";      // pop into rdx - op2
                            program.code += "sub rax, rdx\n"; // add
                            program.code += "push rax\n";     // push back the result
                            break;
                        case ADD:
                            if (!this.macroStack.empty()) {
                                this.macroStack.peek().addToken(token);
                                break;
                            }
                            program.code += "\n; add\n";
                            // add two numbers from the stack
                            program.code += "pop rax\n";      // pop into rax - op1
                            program.code += "pop rdx\n";      // pop into rdx - op2
                            program.code += "add rax, rdx\n"; // add
                            program.code += "push rax\n";     // push back the result
                            break;
                        case PRINT:
                            if (!this.macroStack.empty()) {
                                this.macroStack.peek().addToken(token);
                                break;
                            }
                            program.code += "\n; print\n";
                            // the char to be printed must be at stack top
                            program.code += "mov rax, 1\n";   // write syscall
                            program.code += "mov rdi, 1\n";   // stdout
                            program.code += "mov rsi, rsp\n"; // use char from the stack
                            program.code += "mov rdx, 1\n";   // len=1
                            program.code += "syscall\n";      // syscall
                            break;
                        case POP:
                            if (!this.macroStack.empty()) {
                                this.macroStack.peek().addToken(token);
                                break;
                            }
                            program.code += "\n; pop\n";
                            program.code += "add rsp, 8\n"; // pop without storing the value
                            break;
                        case DUP:
                            if (!this.macroStack.empty()) {
                                this.macroStack.peek().addToken(token);
                                break;
                            }
                            // duplicate
                            program.code += "\n; dup\n";
                            program.code += "mov rax, [rsp]\n"; // get top of the stack
                            program.code += "push rax\n";       // push it
                            break;
                        default:
                            System.out.println(currentFile + ": Unhandled keyword type " + token.text);
                            System.exit(1);
                            break;
                    }
                    break;
                default:
                    System.out.println(currentFile + ": Unhandled token type " + token.text);
                    System.exit(1);
                    break;
            }
        }
        
        if (generateStartAndEnd) {
        //     // set the exit code
            program.code += "global _start\n";
            program.code += "_start:\n";
            program.code += "call main\n";
            program.code += "pop rdi\n";     // get the exit code value
            program.code += "mov rax, 60\n"; // exit syscall
            program.code += "syscall\n";     // syscall
        }

        return program;
    }
}
