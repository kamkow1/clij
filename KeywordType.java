public enum KeywordType {
    ADD("add"), SUB("sub"),
    PRINT("print"),
    POP("pop"), DUP("dup"), SWAP("swap"),
    EQ("eq"), NEQ("neq"),
    LT("lt"), GT("GT"),
    LE("le"), GE("ge"),
    MACRO("macro"), ENDMACRO("endmacro"),
    IF("if"), ENDIF("endif"),
    LOOP("loop"), ENDLOOP("endloop"),
    DO("do"),
    BREAK("break"), CONTINUE("continue"),
    SYSCALL("syscall"),
    SYSCALL1("syscall1"), SYSCALL2("syscall2"),
    SYSCALL3("syscall3"), SYSCALL4("syscall4"),
    SYSCALL5("syscall5"), SYSCALL6("syscall6"),
    INCLUDE("include"),
    MEMREAD("memread"), MEMWRITE("memwrite"),
    MEM("mem"), MEMVAL("memval"),
    MEMALLOC("memalloc"), MEMDEALLOC("dealloc"),
    PROC("proc"), ENDPROC("endproc"),
    ASSIGN("assign");

    private final String name;

    private KeywordType(String s) {
        this.name = s;
    }

    public String toString() {
        return this.name;
    }
}
