public enum TokenType {
    INT("integer"),
    IDENT("identifier"),
    KEYWORD("keyword"),
    STRING("string"),
    DIRECTIVE("directive");

    private final String name;

    private TokenType(String s) {
        this.name = s;
    }

    public String toString() {
        return this.name;
    }
}
