public enum DirectiveType {
    CHARLIT("charlit"), PARAM("param"),
    MEM("mem");

    private final String name;

    private DirectiveType(String s) {
        this.name = s;
    }

    public String toString() {
        return this.name;
    }
}
