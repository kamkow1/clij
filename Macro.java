import java.util.ArrayList;

public class Macro {
    public String name;
    public ArrayList<Token> tokens;

    public Macro(String name, ArrayList<Token> tokens) {
        this.name = name;
        this.tokens = tokens;
    }

    public ArrayList<Token> tokens() {
        return this.tokens;
    }

    public String name() {
        return this.name;
    }

    public void addToken(Token t) {
        this.tokens.add(t);
    }

    public void addTokens(ArrayList<Token> ts) {
        this.tokens.addAll(ts);
    }
}
