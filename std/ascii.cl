-- useful character constants

macro ascii-NUL 0 endmacro
macro ascii-BEL 7 endmacro
macro ascii-BS  8 endmacro
macro ascii-HT  9 endmacro
macro ascii-LF 10 endmacro
macro ascii-VT 11 endmacro
macro ascii-FF 12 endmacro
macro ascii-CR 13 endmacro
