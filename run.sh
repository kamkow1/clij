#!/bin/sh

set -xe

make run > out.asm &&
nasm -felf64 -g -F dwarf ./out.asm &&
ld -dynamic-linker /lib64/ld-linux-x86-64.so.2 -lc -o out ./out.o
