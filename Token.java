public class Token {
    public String text;
    public TokenType type;

    public Token(String text) {
        this.text = text;
    }

    public String toString() {
        return String.format("Token(%s, %s)", this.text, this.type.toString());
    }
}
