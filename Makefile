JAVAC=javac
JAVA=java
sources = $(wildcard *.java)
classes = $(sources:.java=.class)

all: clij

clij: $(classes)

clean:
	rm -rf classes

%.class: %.java
	@mkdir -p classes
	$(JAVAC) $< -d classes

run:
	@CLIJSTD=./std $(JAVA) -cp ./classes Main test/test1.cl

.PHONY: all clij clean run


