import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class Main {
    static void usage() {
        System.out.println("Usage:");
        System.out.println("java -jar clij.jar source-file.cl");
    }

    static void error(String reason) {
        System.out.println("ERROR: " + reason);
    }

    public static void main(String[] args) throws IOException {
        if (args.length < 1) {
            usage();
            error("No source file provided");
            System.exit(1);
        }
        
        String source_file_path = args[0];

        File file = new File(source_file_path);
        FileInputStream fis = new FileInputStream(file);
        byte[] data = new byte[(int)file.length()];
        fis.read(data);
        fis.close();
        String source_file = new String(data, "UTF-8");

        Tokenizer tokenizer = new Tokenizer();
        ArrayList<Token> tokens = tokenizer.tokenize(source_file);

        AsmGenerator asmGen = new AsmGenerator();
        Program program = asmGen.generate(source_file_path, tokens, true);
        System.out.println(program.bss + "\n" + program.code + "\n" + program.data);

        // for (int i = 0; i < tokens.size(); i++) {
        //     System.out.println(tokens.get(i));
        // }
    }
}
